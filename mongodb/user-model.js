const mongoose = require('mongoose')
const Schema = mongoose.Schema

// => create new schema
const User = new Schema({
  name: {
    type: String,
    required: true,
    default: ''
  },
  age: {
    type: Number,
    min: 10,
    max: 70,
    default: 18
  },
  position: {
    type: String,
    default: ''
  },
  phones: {
    type: [Number],
    default: []
  },
  isMarried: {
    type: Boolean,
    default: false
  }
})

// => connect new schema
mongoose.model('users', User)