const mongoose = require('mongoose')
require('./user-model')
const users = require('./users')

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/staff', {
  useMongoClient: true
})
  .then(() => console.log(`Mongoose connected to MongoDB`))
  .catch(err => console.log(err))

const User = mongoose.model('users')

const user = new User({
  name: 'John',
  age: 21,
  position: 'Senior Developer',
  phones: [89515008474]
})

function saveOneUser() {
  user.save()
    .then(data => console.log(data))
    .catch(err => console.log(err))
}

function saveAllUsers() {
  users.forEach(el => {
    new User(el).save()
  })
}

function findUser() {
  User.find({ name: 'John' }).then(data => console.log(JSON.stringify(data, null, 4)))
}

function findSeveralUsers() {
  User.find({ position: { '$in': [ 'Junior Developer', 'Middle Developer' ] }, age: { $gt: 18, $lt: 20 } })
    .limit(4) // => limit quantity output
    .sort('age') // => sort output by field age
    .then(data => console.log(JSON.stringify(data, null, 4)))
    .catch(err => console.log(err))
}

function deleteUser() {
  User.find({ name: 'John' })
    .then(data => {
      let user = data[0]
      console.log(user)
      //
      User.find({ _id: user._id })
        // .then(data => console.log(data))s
        .remove()
        .then(() => console.log(`User removed!`))
    })
}

// saveOneUser()
// saveAllUsers()
// findUser()
// findSeveralUsers()
// deleteUser()
findUser()
