const users = [
  {
    name: 'Chris',
    age: 31,
    position: 'Senior Developer',
    phones: [89515008473]
  },
  {
    name: 'Mike',
    age: 24,
    position: 'Junior Developer',
    phones: [89546008474]
  },
  {
    name: 'Stephany',
    age: 18,
    position: 'Junior Developer',
    phones: [89515108474]
  },
  {
    name: 'Anna',
    age: 19,
    position: 'Middle Developer',
    phones: [89522008272]
  }
]

module.exports = users